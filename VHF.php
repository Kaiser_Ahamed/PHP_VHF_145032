<?php
// VHF--> floatval
// int-->float-->stop when get any character.
//Given output from string variable -> 123.01

$var = '123.01abc';

echo "Float value is :" . (floatval($var));

echo "<br/>";
echo "<br/>";
echo "<br/>";

// VHF--> empty
// Empty --> it's shows nothing but has invisible value
//empty can be int-10,float-12.23,string-"the",array(),boolean-true/false,variable without assign a data-$var;

$data = "";
if (empty($data)) {
    echo "It's Empty !";
} else {
    echo "Nothing Found !";
};

echo "<br/>";
echo "<br/>";
echo "<br/>";

//VHF--> is_array()
//define variable array or not.

$arr = array('I', 'love', 'cricket');
//$arr= 'I love cricket';
if (is_array($arr)) {
    echo "yes! it's an array ";
} else {
    echo "No!";
};

echo "<br/>";
echo "<br/>";
echo "<br/>";

// VHF-->NULL
//define variable NULL or not.

$a = NULL;
if (is_null($a)) {
    echo "It's null !";
} else {
    echo "Nothing is NULL !";
};


echo "<br/>";
echo "<br/>";
echo "<br/>";

//VHF-->serialize, unserialize

$ser = serialize(array('Kaiser', 23, 'Science', 'A+'));
echo $ser . "<br/>" . "<br/>";
$unser = unserialize($ser);
print_r($unser);

echo "<br/>";
echo "<br/>";
//VHF-->var_dump-->define variable type and count data number;
var_dump($unser);

echo "<br/>";
echo "<br/>";
//VHF-->gettype-->define type of variable
echo gettype($unser);

echo "<br/>";
echo "<br/>";
//is_bool-->define true or false-->only int 0 provide false and enrything is true;
$bool=true;
echo is_bool($bool);
